# Дополнить декоратор сache поддержкой max_limit. Алгоритм кеширования LFU

import functools
import requests


def cache(max_limit):
    def internal(f):
        @functools.wraps(f)
        def deco(*args):

            # if the URL is in the cache, increase the number of requests and return the content from the cache
            if args in deco._cache:
                deco._cache[args][0] += 1
                return deco._cache[args][2]

            else:
                # increasing the counter of added items
                deco._number += 1
                # perform a function to get content by url
                result = f(*args)

                # if the URL is not in the cache, check the fullness of the cache.
                if len(deco._cache) >= max_limit:

                    # find the URL with the minimum number of requests and delete it
                    del deco._cache[min(deco._cache, key=deco._cache.get)]

                # add a new URL to the cache with the number of requests 1, addition number and the content
                deco._cache[args] = [1, deco._number, result]
                return result
        deco._cache = {}
        deco._number = 0
        return deco
    return internal


@cache(max_limit=5)
def fetch_url(url, first_n=50):
    """Fetch a given url"""
    res = requests.get(url)
    return res.content[:first_n] if first_n else res.content


fetch_url('https://google.com')
fetch_url('https://google.com')
fetch_url('https://google.com')
fetch_url('https://ithillel.ua')
fetch_url('https://dou.ua')
fetch_url('https://dou.ua')
fetch_url('https://ain.ua')
fetch_url('https://youtube.com')
fetch_url('https://youtube.com')
fetch_url('https://youtube.com')
fetch_url('https://youtube.com')
fetch_url('https://youtube.com')
fetch_url('https://reddit.com')
fetch_url('https://about.gitlab.com')
print(fetch_url._cache)
